﻿namespace HF_API_Tool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listOutput = new System.Windows.Forms.ListBox();
            this.btnGetUserInfo = new System.Windows.Forms.Button();
            this.checkHideKey = new System.Windows.Forms.CheckBox();
            this.txtApiKey = new System.Windows.Forms.TextBox();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.lblAPIKEY = new System.Windows.Forms.Label();
            this.lblUserID = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listOutput
            // 
            this.listOutput.FormattingEnabled = true;
            this.listOutput.HorizontalScrollbar = true;
            this.listOutput.IntegralHeight = false;
            this.listOutput.Location = new System.Drawing.Point(12, 12);
            this.listOutput.Name = "listOutput";
            this.listOutput.Size = new System.Drawing.Size(371, 160);
            this.listOutput.TabIndex = 0;
            // 
            // btnGetUserInfo
            // 
            this.btnGetUserInfo.Font = new System.Drawing.Font("Consolas", 8F);
            this.btnGetUserInfo.Location = new System.Drawing.Point(289, 202);
            this.btnGetUserInfo.Name = "btnGetUserInfo";
            this.btnGetUserInfo.Size = new System.Drawing.Size(94, 22);
            this.btnGetUserInfo.TabIndex = 1;
            this.btnGetUserInfo.Text = "Get User Info";
            this.btnGetUserInfo.UseVisualStyleBackColor = true;
            this.btnGetUserInfo.Click += new System.EventHandler(this.btnGetUserInfo_Click);
            // 
            // checkHideKey
            // 
            this.checkHideKey.AutoSize = true;
            this.checkHideKey.Font = new System.Drawing.Font("Consolas", 8F);
            this.checkHideKey.Location = new System.Drawing.Point(368, 180);
            this.checkHideKey.Name = "checkHideKey";
            this.checkHideKey.Size = new System.Drawing.Size(15, 14);
            this.checkHideKey.TabIndex = 2;
            this.checkHideKey.UseVisualStyleBackColor = true;
            this.checkHideKey.CheckedChanged += new System.EventHandler(this.checkHideKey_CheckedChanged);
            // 
            // txtApiKey
            // 
            this.txtApiKey.Font = new System.Drawing.Font("Consolas", 8F);
            this.txtApiKey.Location = new System.Drawing.Point(79, 177);
            this.txtApiKey.Name = "txtApiKey";
            this.txtApiKey.Size = new System.Drawing.Size(283, 20);
            this.txtApiKey.TabIndex = 3;
            // 
            // txtUserID
            // 
            this.txtUserID.Font = new System.Drawing.Font("Consolas", 8F);
            this.txtUserID.Location = new System.Drawing.Point(79, 203);
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(204, 20);
            this.txtUserID.TabIndex = 4;
            // 
            // lblAPIKEY
            // 
            this.lblAPIKEY.AutoSize = true;
            this.lblAPIKEY.Font = new System.Drawing.Font("Consolas", 9F);
            this.lblAPIKEY.Location = new System.Drawing.Point(12, 180);
            this.lblAPIKEY.Name = "lblAPIKEY";
            this.lblAPIKEY.Size = new System.Drawing.Size(63, 14);
            this.lblAPIKEY.TabIndex = 5;
            this.lblAPIKEY.Text = "API Key:";
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Font = new System.Drawing.Font("Consolas", 9F);
            this.lblUserID.Location = new System.Drawing.Point(12, 206);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(63, 14);
            this.lblUserID.TabIndex = 6;
            this.lblUserID.Text = "User ID:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 231);
            this.Controls.Add(this.lblUserID);
            this.Controls.Add(this.lblAPIKEY);
            this.Controls.Add(this.txtUserID);
            this.Controls.Add(this.txtApiKey);
            this.Controls.Add(this.checkHideKey);
            this.Controls.Add(this.btnGetUserInfo);
            this.Controls.Add(this.listOutput);
            this.Name = "frmMain";
            this.Text = "HF API Tool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listOutput;
        private System.Windows.Forms.Button btnGetUserInfo;
        private System.Windows.Forms.CheckBox checkHideKey;
        private System.Windows.Forms.TextBox txtApiKey;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Label lblAPIKEY;
        private System.Windows.Forms.Label lblUserID;
    }
}

