using System;
using System.Net;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace HF_API_Tool {
    public partial class frmMain : Form {

        public hfAPI HF = new hfAPI();
        public frmMain() {
            InitializeComponent();
        }

        private void checkHideKey_CheckedChanged(object sender, EventArgs e) {
            if (checkHideKey.Checked) {
                txtApiKey.PasswordChar = '•';
            } else {
                txtApiKey.PasswordChar = '\0';
            }
        }

        private void btnGetUserInfo_Click(object sender, EventArgs e) {
            listOutput.Items.Clear();
            int userID;
            Int32.TryParse(txtUserID.Text, out userID);
            getUserInfo(txtApiKey.Text, userID, listOutput);
        } 

        public void getUserInfo(string key, int userID, ListBox output) {
            HF.apiKey = key;
            UserProfile nUser = HF.GetUserInfo(userID);
            output.Items.Add("Username: " + nUser.username);
            output.Items.Add("Post Count: " + nUser.postCount);
            output.Items.Add("Avatar URL: " + nUser.avatarImg);
            output.Items.Add("Avatar Type: " + nUser.avatarType);
            output.Items.Add("Usergroup ID: " + nUser.userGroup);
            output.Items.Add("Displaygroup ID: " + nUser.displayGroup);
            output.Items.Add("Usertitle: " + nUser.usertitle);
            output.Items.Add("Time Online: " + nUser.timeOnline);
            output.Items.Add("Reg Date: " + nUser.regDate);
            output.Items.Add("Last Active: " + nUser.lastOnline);
            output.Items.Add("Reputation: " + nUser.reputation);
        }
    }
}

public class hfAPI {
    private WebClient nWbx = new WebClient();
    private string apiBase = "https://hackforums.net/api/v1/";
    public string apiKey;

    private JObject GetAPIResponse(string api) {
        JObject jResponse = new JObject();
        nWbx.Credentials = new NetworkCredential(apiKey, "");
        nWbx.Headers.Add("user-agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201");
        jResponse = JObject.Parse(nWbx.DownloadString(apiBase + api));
        return jResponse;
    }

    public UserProfile GetUserInfo(int id) {
        UserProfile nUserInfo = new UserProfile();
        try  {
            JObject apiResponse = GetAPIResponse("user/" + id);
            nUserInfo.username = (string)apiResponse.SelectToken("result").SelectToken("username");
            nUserInfo.postCount = (int)apiResponse.SelectToken("result").SelectToken("postnum");
            nUserInfo.avatarImg = (string)apiResponse.SelectToken("result").SelectToken("avatar");
            nUserInfo.avatarType = (string)apiResponse.SelectToken("result").SelectToken("avatartype");
            nUserInfo.userGroup = (int)apiResponse.SelectToken("result").SelectToken("usergroup");
            nUserInfo.displayGroup = (int)apiResponse.SelectToken("result").SelectToken("displaygroup");
            nUserInfo.usertitle = (string)apiResponse.SelectToken("result").SelectToken("usertitle");
            nUserInfo.timeOnline = (string)apiResponse.SelectToken("result").SelectToken("timeonline");
            nUserInfo.regDate = (string)apiResponse.SelectToken("result").SelectToken("regdate");
            nUserInfo.lastOnline = (string)apiResponse.SelectToken("result").SelectToken("lastactive");
            nUserInfo.reputation = (int)apiResponse.SelectToken("result").SelectToken("reputation");
        } catch (Exception ex) {
            MessageBox.Show(ex.Message);
        }
        return nUserInfo;
    }
}

public class UserProfile {
    public string username;
    public int postCount;
    public string avatarImg;
    public string avatarType;
    public int userGroup;
    public int displayGroup;
    public string usertitle;
    public string timeOnline;
    public string regDate;
    public string lastOnline;
    public int reputation;
}
